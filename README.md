# Spellcheck

Spelcheck Project has 2 main components:
* Simple PHP frontend
* Apache Solr (version 7.6) backend

How to run the project:
1. Unzip folder solr.zip
2. Run solr via command prompt (navigate to *bin* folder and run command *solr.cmd start*)
3. Run PHP frontend via command prompt (php -S localhost:8000)
4. Navigate to http://localhost:8000
5. Search

You can also access Solr by default at http://localhost:8983/solr/

Note: Project was developed for Windows machine

Prerequsities for Solr:
*  Java 8 or greater

Resources:
* Solr: https://lucene.apache.org/solr/

