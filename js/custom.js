$(document).ready(function () {
  $(function () {
    $("form").on("submit", function (e) {
      e.preventDefault();
      // Retrieve the search field value
      const trimSearch = $.trim($("#searchTerm").val());
      const search = trimSearch.replace(/\s/g, "&");
      // Check if its empty of not
      if (search === "") {
        $(".response").html("<p class='error'>Please fill in search term</p>");
        return false;
      } else if (search.length < 4) {
        $(".response").html("<p class='error'>Please fill in search at least 4 characters</p>");
      } else {
        // Remove error message
        $(".response").html("");
        // Do Ajax call
        $.ajax({
          type: "post",
          url: "search.php",
          dataType: "json",
          data: "searchTerm=" + search,
          success: function (response) {
            if (response.success === true) {
              $(".response").html("Search term found: <p class='success'>" + response.result + "</p>");
            } else if (response.suggestion === true) {
              $(".response").html("0 results found, did you mean: <a href='' class='suggestions'>" + response.result + "</a> ?");
              // Do Ajax call on suggestion click
              $(".suggestions").click(function (e) {
                e.preventDefault();
                // Do Ajax call
                $.ajax({
                  type: "post",
                  url: "search.php",
                  dataType: "json",
                  data: "searchTerm=" + response.result,
                  success: function (response) {
                    $("#searchTerm").val(response.result);
                    $(".response").html("Search term found: <p class='success'>" + response.result + "</p>");
                  },
                  error: function () {
                    $(".response").html("<p class='error'>Some unexpected error occured</p>");
                  }
                });
              });
            } else {
              $(".response").html("<p class='error'>0 results found, please try with different search term</p>");
            }
          },
          error: function () {
            $(".response").html("<p class='error'>Some unexpected error occured</p>");
          }
        });
      }
    });
  });
});