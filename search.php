<?php
// Get search term from Ajax post
if (isset($_POST["searchTerm"])) {
    $search = $_POST["searchTerm"];
} else {
    $search = "";
}
$ch = curl_init();

// Set URL to Solr engine with spellcheck parameter
curl_setopt($ch, CURLOPT_URL, "http://localhost:8983/solr/brands/spell?q=name:" . $search . "&spellcheck=on");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
// Receive response from solar
$solr_response = curl_exec($ch);
curl_close($ch);
$response = json_decode($solr_response, true);
// Parse response
if ($response["response"]["numFound"] > 0) {
    $responseCount = $response["response"]["numFound"];
    // Return all results
    for ($x = 0; $x < $responseCount; $x++) {
        $result[$x] = $response["response"]["docs"][$x]["name"][0];
    }
    echo json_encode($solrResponse = [
        "success" => true,
        "suggestion" => false,
        "result" => $result,
    ]);
} else {
    // Check if there Solr offer any suggestions
    if (!empty($response["spellcheck"]["suggestions"])) {
        // Return only first suggeston
        $suggestion = $response["spellcheck"]["suggestions"][1]["suggestion"][0]["word"];
        echo json_encode($solrResponse = [
            "success" => false,
            "suggestion" => true,
            "result" => $suggestion,
        ]);
    } else {
        echo json_encode($solrResponse = [
            "success" => false,
            "suggestion" => false,
            "result" => "",
        ]);
    }
}
